raise 'Yuki::SoftReset is not defined' unless defined?(Yuki::SoftReset)
raise 'Graphics.window is not defined' unless defined?(Graphics.window)
raise 'Input.swap_states is not defined' unless defined?(Input.swap_states)
raise 'Input.get_text is not defined' unless defined?(Input.get_text)
raise 'UI::BlurScreenshot is not defined' unless defined?(UI::BlurScreenshot)
raise 'This plugin works only after .25.04' if PSDK_Version < 6404
