unless PARGV[:worldmap] || PARGV[:"animation-editor"] || PARGV[:test] || PARGV[:tags]
  Scheduler.__remove_task(:on_update, :any, 'SoftReset', 10**99)
  # Add soft reset sequence
  Scheduler.add_proc(:on_update, :any, 'SoftReset', 10**99) do
    if Input::Keyboard.press?(Input::Keyboard::F12) && $scene.class != Yuki::SoftReset
      next if Yuki::SoftReset.show_message

      # Set the running to false if possible
      $scene&.instance_variable_set(:@running, false)
      # Switching the scene to the soft reset
      $scene = Yuki::SoftReset.new
      # Telling soft reset is processing
      cc 0x03
      puts 'Soft resetting 2...'
      cc 0x07
      raise Reset, ''
    end
  end

  module Yuki
    class SoftReset
      class << self
        # Show the soft reset message
        # @return [Boolean] if the soft reset should not happen
        def show_message
          result = false
          vp = Viewport.create(:main, 50_000)
          st = UI::SpriteStack.new(vp)
          st.add_custom_sprite(to_dispose = UI::BlurScreenshot.new(vp, $scene)) if $scene
          st.add_text(0, 0, 0, 16, "If you really want to soft reset press Y,\notherwise press any other key", 0, 1, color: 9)
          loop do
            Input.swap_states
            Graphics.window.update
            txt = Input.get_text
            next unless txt

            result = true if txt.downcase != 'y'
            break
          end
          to_dispose.dispose
          vp.dispose
          return result
        end
      end
    end
  end
end
